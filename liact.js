import { html, render } from "https://unpkg.com/lit-html?module";

export class Props extends Map {
  getBoolean(key) {
    const value = this.get(key);
    if (value == null) {
      return undefined;
    }
    return !(["false", "no", "off"].includes(value));
  }
  getNumber(key) {
    const value = this.get(key);
    if (value == null) {
      return undefined;
    }
    return Number(value);
  }
  getString(key) {
    return this.get(key);
  }
  getListOf(f, key) {
    const value = this.get(key);
    if (value == null) {
      return undefined;
    }
    return value.split(" ").map(f);
  }
}

export function component(view, options = {}) {
  const base = (options.base && options.base.prototype instanceof HTMLElement) ? options.base : HTMLElement;
  return class extends base {
    constructor() {
      super();
      this.refs = [];
      this.states = [];
      this.effects = [];
      this.cleanups = [];
      this._view = view;
      this.attachShadow({ mode: "open" });
      this.render();
    }
    get props() {
      return new Props(this.getAttributeNames().map(name => [name, this.getAttribute(name)]));
    }
    get slots() {
      return Array.from(this.shadowRoot.querySelectorAll("slot") ?? [])
    }
    render() {
      // cleanup all effects
      for (let cleanup of this.cleanups) {
        cleanup();
      }
      this.cleanups = [];
      
      // reset index
      this.index = {
        ref: 0,
        state: 0,
        effect: 0,
      };
      
      // view
      const template = this._view(this, { props: this.props, shadowRoot: this.shadowRoot, slots: this.slots });
      render(template, this.shadowRoot);
      
      // handle effects
      for (let { f, deps, oldDeps, isInitialCall } of this.effects) {
        const hasSomethingChanged = (deps, oldDeps) =>
          deps.length !== oldDeps.length || deps.some((dep, i) => dep !== oldDeps[i]);
        if (deps === undefined || isInitialCall || hasSomethingChanged(deps, oldDeps)) {
          this.effects[this.index.effect] = {
            f,
            deps,
            oldDeps: deps,
            isInitialCall: false,
          };
          const onCleanup = f();
          if (typeof onCleanup === "function") {
            this.cleanups.push(onCleanup);
          }
        }
      }
    }
  };
}

export function useRef(self, initial) {
  const index = self.index.ref;
  const isInitialCall = index >= self.refs.length;
  if (isInitialCall) {
    self.refs[index] = { current: initial };
  }
  return self.refs[self.index.ref++];
};

export function useState(self, initial) {
  const index = self.index.state;
  const isInitialCall = index >= self.states.length;
  const setState = value => {
    if (self.states[index] === value) {
      return;
    }
    self.states = self.states.map(
      (state, i) => index === i ? value : state
    );
    self.render();
  };
  if (isInitialCall) {
    self.states[index] = initial;
  }
  return [self.states[self.index.state++], setState];
};

export function useEffect(self, f, deps = undefined) {
  const index = self.index.effect;
  const isInitialCall = index >= self.effects.length;
  const oldDeps = isInitialCall ? [] : self.effects[index].deps;
  self.effects[index] = { f, deps, oldDeps, isInitialCall };
  self.index.effect++;
  return;
}

export function useCallback(self, f, deps = []) {
  const [callback, setCallback] = useState(self, null);
  const [oldDeps, setOldDeps] = useState(self, []);
  if (callback !== null && deps.length === oldDeps.length && deps.every((dep, i) => dep === oldDeps[i])) {
    return callback;
  }
  setCallback(f);
  setOldDeps(deps);
  return f;
}

export function useMemo(self, f, deps = []) {
  const [value, setValue] = useState(self, void 0);
  const [oldDeps, setOldDeps] = useState(self, deps);
  if (value !== undefined && deps.length === oldDeps.length && deps.every((dep, i) => dep === oldDeps[i])) {
    return value;
  }
  const v = f();
  setValue(v);
  setOldDeps(deps);
  return v;
}

