// hello-world.js

import { html } from "https://unpkg.com/lit-html?module";
import {
  component,
  useState,
  useEffect
} from "../../liact.js";

const Language = (code, name) => ({ code, name });

const languages = [
  Language("En-US", "English"),
  Language("Ja-JP", "日本語"),
];

const texts = {
  "En-US": {
    "Hello, world!": "Hello, world!",
    "Liact Test": "Liact Test",
  },
  "Ja-JP": {
    "Hello, world!": "こんにちは、世界！",
    "Liact Test": "Liact テスト",
  },
};

const getText = lang => (id, fallback = "") =>
  texts[lang] ? (texts[lang][id] ?? fallback) : fallback;

// Create component and register it as a HTML Custom Element
customElements.define("hello-world", component((self, { props }) => {
  const [lang, setLang] = useState(self, props.get("lang") ?? "En-US");
  const T = getText(lang);
  useEffect(self, () => {
    document.title = T("Liact Test");
  }, [lang]);
  return html`
    <h1>${T("Liact Test")}</h1>
    <select @change=${ev => setLang(ev.target.value)}>${
      languages.map(({ code, name }) => html`
        <option value="${code}" ?selected=${code == lang}>${name}</option>
      `)
    }</select>
    <p>${T("Hello, world!")}</p>
  `;
}));
