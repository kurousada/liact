import { html } from "https://unpkg.com/lit-html?module";
import hljs from "https://unpkg.com/@highlightjs/cdn-assets/es/highlight.min.js";
import "https://unpkg.com/markdown-it/dist/markdown-it.min.js";
import {
  component,
  useRef,
  useEffect
} from "https://unpkg.com/liact";

const markdown = ({
  allowUnsafeHtml = false,
  linkify = true,
  highlight = true,
} = {}) => window.markdownit({
  html: allowUnsafeHtml,
  linkify,
  highlight: !highlight ? undefined : function (str, lang) {
    if (lang && hljs.getLanguage(lang)) {
      try {
        return `<pre><code class="hljs">${hljs.highlight(str, { language: lang }).value}</code></pre>`;
      } catch (__) {}
    }

    return `<pre><code>${src}</code></pre>`; // use external default escaping
  }
});

customElements.define("markdown-it", component((self, { props, shadowRoot, slots }) => {
  const src = props.get("src");
  const theme = props.get("theme") ?? "default";
  const languages = props.getListOf(String, "languages") ?? [];
  const allowUnsafeHtml = props.getBoolean("allow-unsafe-html") ?? false;
  const linkify = props.getBoolean("linkify") ?? true;
  const highlight = props.getBoolean("highlight") ?? true;
  const root = useRef(self, null);
  const template = useRef(self, null);
  useEffect(self, () => {
    root.current = shadowRoot.querySelector(".content");
    if (slots.length < 1) {
      return;
    }
    const assignedNodes = slots[0].assignedNodes() ?? [];
    if (assignedNodes.length < 1) {
      return;
    }
    template.current = assignedNodes.find(node => node.tagName === "TEMPLATE");
  }, [shadowRoot, slots]);
  useEffect(self, async () => {
    if (!src || !root.current) {
      return;
    }
    const resp = await fetch(src).catch(e => {
      console.error(e);
    });
    const file = await resp.text().catch(e => {
      console.error(e);
    });
    const md = markdown({ allowUnsafeHtml, linkify, highlight }).render(file);
    root.current.innerHTML = md;
  }, [src, root.current, allowUnsafeHtml, linkify, highlight]);
  useEffect(self, () => {
    if (!template.current || !root.current) {
      return;
    }
    const md = markdown({ allowUnsafeHtml, linkify, highlight }).render(template.current.textContent);
    root.current.innerHTML = md;
  }, [template.current, allowUnsafeHtml, linkify, highlight]);
  useEffect(self, async () => {
    if (!highlight || !languages) {
      return;
    }
    for (let language of languages) {
      if (!hljs.getLanguage(language)) {
        const { default: defaultExports } =
          await import(`https://unpkg.com/@highlightjs/cdn-assets/es/languages/${language}.min.js`)
            .catch(e => console.error(`unable to load ${language}: `, e));
        hljs.registerLanguage(language, defaultExports);
        if (hljs.getLanguage(language)) {
          self.render();
        }
      }
    }
  }, [languages]);
  return html`
    <style>
      p, li, div, blockquote {
        overflow-wrap: break-word;
      }
      p {
        color: rgba(0,0,0,.9);
        line-height: 1.2em;
      }
      p a {
        color: rgba(0,0,0,.9);
        text-decoration: underline;
      }
      pre {
        overflow-x: scroll;
        ${ highlight ? "" : "background: rgba(0,0,0,.1);" }
      }
      code {
        padding: .1em;
        background: rgba(0,0,0,.1);
        line-height: 1.3em;
      }
      pre code {
        background: transparent;
      }
      blockquote {
        padding: .5em 1em;
        margin: auto 0;
        background: rgba(0,0,0,.1);
        border-left: .5em solid rgba(0,0,0,.1);
      }
      ul {
        padding-left: 2em;
      }
      h4 {
        color: rgb(0,0,0,.6);
      }
      table {
        margin: auto 0;
        width: 100%;
        border-collapse: collapse;
      }
      tr {
        border-bottom: 1px solid rgba(0,0,0,.2);
      }
      thead tr {
        border-bottom: none;
        background: rgba(0,0,0,0.1);
      }
      th {
        text-align: left;
        font-weight: normal;
        font-size: 90%;
      }
      td {
        color: rgba(0,0,0,.8);
        font-size: 95%;
      }
      th,
      td {
        padding: .5em;
      }
    </style>
    <slot></slot>
    <div class="content"></div>
    ${ !highlight ? "" : html`<link rel="stylesheet" href="https://unpkg.com/@highlightjs/cdn-assets/styles/${theme}.min.css">` }
  `;
}));

