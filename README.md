# Liact

**English** | [日本語](https://kurousada.gitlab.io/liact?lang=Ja-JP)

> lit-html + React syntax = Standard & Maintainable

Write Web Components declarative using React-like syntax.

Based on [lit-html](https://lit.dev) and supports both plain JavaScript and TypeScript.

## Example

Write your components using [lit-html](https://lit.dev).

Here we use [unpkg.com](https://unpkg.com) to avoid installation.

```js
// hello-world.js

import { html } from "https://unpkg.com/lit-html?module";
import {
  component,
  useState,
  useEffect
} from "https://unpkg.com/liact";

const languageOf = (code, name) => ({ code, name });

const languages = [
  languageOf("En-US", "English"),
  languageOf("Ja-JP", "日本語"),
];

const texts = {
  "En-US": {
    "Hello, world!": "Hello, world!",
    "Liact Test": "Liact Test",
  },
  "Ja-JP": {
    "Hello, world!": "こんにちは、世界！",
    "Liact Test": "Liact テスト",
  },
};

const getText = lang => (id, fallback = "") =>
  texts[lang] ? (texts[lang][id] ?? fallback) : fallback;

// Create component and register it as a HTML Custom Element
customElements.define("hello-world", component((self, props) => {
  const [lang, setLang] = setState(self, props.get("lang") ?? "En-US");
  const T = getText(lang);
  useEffect(self, () => {
    document.title = T("Liact Test");
  }, [lang]);
  return html`
    <h1>Liact</h1>
    <select @change=${ev => setLang(ev.target.value)}>${
      languages.map(({ code, name }) => html`
        <option value="${code}" ?selected=${code == lang}>${name}</option>
      `)
    }</select>
    <p>${T("Hello, world!")}</p>
  `;
}));
```

Then load and use the Custom Element you created.

```html
<!-- index.html -->

<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
  </head>
  <body>
    <hello-world lang="En-US"></hello-world>
    <script type="module" src="./hello-world.js"></script>
  </body>
</html>
```

Now run a server to host the files.

```sh
python3 -m http.server
```

And open [http://0.0.0.0:8000](http://0.0.0.0:8000) on your browser to see the result.

You can see this:

<video muted autoplay loop>
  <source type="video/webm" src="./examples/hello-world/screenshot.webm">
  Unable to play video because your browser doesn't seem to support WebM format.
</video>

## Installation

No need to install if you use [unpkg.com](https://unpkg.com) as you see in the example above.

### Unpkg.com (Recommended)

Directly import from [https://unpkg.com/liact](https://unpkg.com/liact).

```js
import { component } from "https://unpkg.com/liact";
```

or

```html
<script type="module" src="https://unpkg.com/liact"></script>
```

### NPM (or Yarn)

Install via CLI:

```sh
npm install --save-dev liact
```

or, if you prefer to yarn:

```sh
yarn add liact
```

Then import "liact".

```js
import { component } from "liact";
```

## TypeScript

Type declaration file for TypeScript (liact.d.ts) is built-in if you install npm package.

## Documentation

See [https://kurousada.gitlab.io/liact](https://kurousada.gitlab.io/liact)

To see the source codes, visit [https://gitlab.com/kurousada/liact](https://gitlab.com/kurousada/liact).

## API

### type Props

Key-value Map of properties given to the element.

```typescript
export interface Props extends Map<string, string> {
  getBoolean(key: string): Boolean | undefined;
  getNumber(key: string): Number | undefined;
  getString(key: string): String | undefined;
  getListOf<T>(f: (value: string) => T, key: string): T[] | undefined;
};
```

Note that `Props.prototype.getBoolean` will return `false` for "false", "no" and "off", `true` if any other value is set.

#### Example

```js
const myLanguages = component((self, { props }) => {
  const languages = props.getListOf(String, "languages") ?? [];
  const [language, setLanguage] = useState(self, "English");
  return html`
    <select @change=${ev => setLanguage(ev.target.value)}>${
      languages.map(lang => html`
        <option value="lang">${lang}</option>
      `)
    }</select>
  `;
});
```

### type Component

Object represents Web Component.

*Do not use this object directly* and use `ViewArgs` given as an argument to the view function.

```typescript
export type Component = unknown
```

### type ViewArgs

Type of a object given to view function as its argument.

```typescript
export interface ViewArgs {
  readonly props     : Props;
  readonly shadowRoot: ShadowRoot;
  readonly slots     : HTMLSlotElement[];
           render    : () => void;
}
```

#### Members & Methods

| Name       | Type              | Description                  |
|------------|-------------------|------------------------------|
| props      | HTMLElement       | Props given to the component |
| shadowRoot | ShadowRoot        | Shadow root of the component |
| slots      | HTMLSlotElement[] | Slots in the component       |
| render     | () => void        | Render function of component |

### type View

Type of a function represents view.

```typescript
export type View = (self: Component, args: ViewArgs) => TemplateResult;
```

#### Arguments

| Name | Type      | Description                     |
|------|-----------|---------------------------------|
| self | Component | Used for hooks                  |
| args | ViewArgs  | Props and others (see ViewArgs) |

#### Returns

| Type           | Description                                  |
|----------------|----------------------------------------------|
| TemplateResult | Usually created using `html` from "lit-html" |

### type ComponentOptions

Object represents options for `component()`.

```typescript
export interface ComponentOptions {
  base: Optional<HTMLElement>;
}
```

#### Members & Methods

| Name    | Type                  | Description                  |
|---------|-----------------------|------------------------------|
| base    | Optional<HTMLElement> | Base object of the component |

### funtion component()

Create Web Component

```typescript
export function component<T extends HTMLElement = HTMLElement>(view: View, options: ComponentOptions = {}): T;
```

#### Arguments

| Name    | Type             | Description   |
|---------|------------------|---------------|
| view    | View             | View function |
| options | ComponentOptions | Options       |

#### Returns

| Type      | Description                               |
|-----------|-------------------------------------------|
| Component | Usually passed to `customElements.define` |


#### Example

```js
import { component } from "liact";

// Create component
const helloMessage = component((self, { props }) => html`
  <p>Hello, ${props.get("name") ?? "anonymous"}!</p>
`);

// Register as a HTML Custom Element
customElements.define("hello-message", helloMessage);
```

### type SetState<T>

Type of function to set & update state created by `useState()`.

```typescript
export type SetState<T> = (value: T) => void;
```

#### Arguments

| Name  | Type | Description            |
|-------|------|------------------------|
| value | T    | New value of the state |

### function useState()

Hook to use state.
The associated component will be re-rendered when the state changes.

```typescript
export function useState<T>(self: Component, initial: T): [T, SetState<T>];
```

#### Arguments

| Name    | Type      | Description                |
|---------|-----------|----------------------------|
| self    | Component | Used for hooks             |
| initial | T         | Initial value of the state |

#### Returns

| Type             | Description                                    |
|------------------|------------------------------------------------|
| [T, SetState<T>] | A pair of current state and function to update |

### type Ref<T>

Type of object represents a mutable reference to a value.

```typescript
export interface Ref<T> {
  current: T;
}
```

#### Members & Methods

| Name    | Type | Description          |
|---------|------|----------------------|
| current | T    | Current value of Ref |

### function useRef()

Hook to use Ref.
The associated component will **not** be re-rendered even when the ref changes.

```typescript
export function useRef<T>(self: Component, initial: T): Ref<T>;
```

#### Arguments

| Name    | Type      | Description              |
|---------|-----------|--------------------------|
| self    | Component | Used for hooks           |
| initial | T         | Initial value of the ref |

#### Returns

| Type   | Description                                   |
|--------|-----------------------------------------------|
| Ref<T> | A ref which is a mutable reference to a value |

### type Cleanup

Type of function to cleanup an effect.
Cleanup functions will be executed on next render of effect.

```typescript
export type Cleanup = () => void;
```

### function useEffect()

Hook to use effect.
Effect will be executed in the end of render phase of the component only when at least one of deps has changed.
Passing `[]` to deps makes one-time effect while not specifying deps cause executions of effect on every render.

```typescript
export function useRef<T>(self: Component, f: () => Cleanup, deps?: any[]): void;
```

#### Arguments

| Name | Type          | Description         |
|------|---------------|---------------------|
| self | Component     | Used for hooks      |
| f    | () => Cleanup | Function to execute |
| deps | any[]         | Dependency values   |

## Contribution

Pull Requests are always welcome!

## License

MIT License

