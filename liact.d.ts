
import { TemplateResult } from "lit-html";

export interface Props extends Map<string, string> {
  getBoolean(key: string): Boolean | undefined;
  getNumber(key: string): Number | undefined;
  getString(key: string): String | undefined;
  getListOf<T>(f: (value: string) => T, key: string): T[] | undefined;
};

export type Component = unknown;

export interface ViewArgs {
  readonly props: Props;
  readonly shadowRoot: ShadowRoot;
  readonly slots: HTMLSlotElement[];
  render(): void;
}

export type View = (self: Component, args: ViewArgs) => TemplateResult;

export interface ComponentOptions {
  base: Optional<HTMLElement>;
}

export function component<T extends HTMLElement = HTMLElement>(view: View, options: ComponentOptions = {}): T;

export interface Ref<T> {
  current: T;
}

export function useRef<T>(self: Component, initial: T): Ref<T>;

export type SetState<T> = (value: T) => void;

export function useState<T>(self: Component, initial: T): [T, SetState<T>];

export type Cleanup = () => void;

export function useEffect(self: Component, f: () => Cleanup, deps?: any[]): void;

export function useCallback<T extends Function>(self: Component, f: T, deps: any[] = []): T;

export function useMemo<T>(self: Component, f: () => T, deps: any[] = []): T;

